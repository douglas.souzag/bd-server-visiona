// Adicionar aqui as configurações do banco de desenvolvimento, homologação e produção.
require('dotenv-safe').config({
  allowEmptyValues: true,
  example: './.env'
})

module.exports = {

  development: {
    client: 'pg',
    connection: {
      host: process.env.DB_HOST,
      database: process.env.DB_DATABASE,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD
    },
    migrations: {
      directory: `${__dirname}/src/database/migrations`
    },
    seeds: {
      directory: `${__dirname}/src/database/seeds`
    }
  }
};
