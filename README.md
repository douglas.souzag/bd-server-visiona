[![Nodejs](https://img.shields.io/badge/nodejs-v12.18.1-<COLOR>.svg)](https://shields.io/)
[![Bcrypt](https://img.shields.io/badge/bcrypt-v5.0.0-red.svg)](https://shields.io/)
[![Express](https://img.shields.io/badge/express-v4.17.1-green.svg)](https://shields.io/)
[![JWT](https://img.shields.io/badge/jsonwebtoken-v8.5.1-blue.svg)](https://shields.io/)
[![Objection](https://img.shields.io/badge/objection-v2.2.1-blue.svg)](https://shields.io/)
[![Knex](https://img.shields.io/badge/knex-v0.21.1-blue.svg)](https://shields.io/)

# BD Server Visiona
API do banco de dados do projeto Visiona. Gitlab: <https://gitlab.com/douglas.souzag/bd-server-visiona>

## Version: 1.0.0

### Terms of service
<http://swagger.io/terms/>

**Contact information:**  
douglas.souzag@hotmail.com  

### /authenticate

#### POST
##### Summary

Realiza a autenticação do sistema

##### Description

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | Objeto do usuário a ser criado | Yes | [UserLogin](#userlogin) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Ok |

### /users

#### GET
##### Summary

Retorna todos os usuários do sistema

##### Description

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| x-access-token | header | Token de autenticação | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Ok |

#### POST
##### Summary

Adiciona um novo usuário

##### Description

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | Objeto do usuário a ser criado | Yes | [User](#user) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Ok |

#### PUT
##### Summary

Altera um usuário já existente

##### Description

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| x-access-token | header | Token de autenticação | Yes | string |
| body | body | Objeto do usuário a ser alterado | Yes | [User](#user) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Ok |

#### DELETE
##### Summary

Deleta um usuário já existente

##### Description

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| x-access-token | header | Token de autenticação | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Ok |

### /areas

#### GET
##### Summary

Retorna todas as areas do usuário

##### Description

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| x-access-token | header | Token de autenticação | Yes | string |
| page | query | Página | No | integer |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Ok |

#### POST
##### Summary

Adiciona uma nova área

##### Description

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| x-access-token | header | Token de autenticação | Yes | string |
| body | body | Objeto da área a ser criada | Yes | [Area](#area) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Ok |

#### DELETE
##### Summary

Deleta uma área já existente

##### Description

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| x-access-token | header | Token de autenticação | Yes | string |
| id | query | ID da área a ser deletada | Yes | integer |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Ok |

### /images

#### GET
##### Summary

Retorna todas as imagens da área

##### Description

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| x-access-token | header | Token de autenticação | Yes | string |
| id | query | ID da área | Yes | integer |
| page | query | Página | No | integer |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Ok |

#### POST
##### Summary

Adiciona uma nova área

##### Description

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| x-access-token | header | Token de autenticação | Yes | string |
| body | body | Objeto da imagem a ser criada | Yes | [Image](#image) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Ok |

### Models

#### UserLogin

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| email | string |  | No |
| password | string |  | No |

#### User

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| username | string |  | No |
| email | string |  | No |
| password | string |  | No |

#### Area

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| title | string |  | No |
| geojson | string |  | No |
| ha | number |  | No |
| cloudiness | number |  | No |
| period | string |  | No |
| satellite | string |  | No |

#### Image

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| area_id | integer |  | No |
| link | string |  | No |
| meta_dados | string |  | No |
