const express = require('express')
const routes = express.Router()

const VerificarToken = require('./middleware/auth')
const UserController = require('./controllers/UserController')
const AreaController = require('./controllers/AreaController')
const ImageController = require('./controllers/ImageController')


//Users
routes.get('/users',VerificarToken,UserController.index)
routes.post('/users',UserController.create) //Rota desprotegida para cadastro de usuario
routes.put('/users',VerificarToken,UserController.update)
routes.delete('/users',VerificarToken,UserController.delete)
//Areas
routes.get('/areas',VerificarToken,AreaController.index)
routes.get('/bigareas', AreaController.getBigAreas)
routes.post('/areas',VerificarToken,AreaController.create)
routes.delete('/areas/:id',VerificarToken,AreaController.delete)
//images
routes.get('/images',VerificarToken,ImageController.index)
routes.get('/images-processed',VerificarToken,ImageController.indexProcessed)
routes.post('/images',VerificarToken,ImageController.create)
routes.post('/download',VerificarToken,ImageController.download)
routes.post('/process',VerificarToken,ImageController.process)
routes.post('/thumbnail',VerificarToken,ImageController.thumbnail)
//Autenticacao
routes.post('/authenticate',UserController.authenticate) //Rota desprotegida para autenticação

module.exports = routes