const knex = require('../database')
const axios = require('axios')
const Image = require('.././models/ImageModel.js');
const Area = require('.././models/AreaModel.js');

module.exports = {
  async index(req, res, next) {
    try {
      const total_per_page = 8

      const { area_id, page = 1 } = req.query
      const user_id = req.user.id
      
      const preQuery = await Area.query()
      .where({id: area_id})
      .limit(1)

      if(preQuery[0].user_id != user_id){
        return res.send({message:"Imagem inacessível"}) 
      }

      const query = Image.query()
      .limit(total_per_page)
      .offset((page - 1) * total_per_page)

      const countQuery = Image.query()
      .count()

      if(area_id){
        query
        .where({area_id: parseInt(area_id)})
        .joinRelated('areas')
      
        countQuery
        .where({area_id})
      }
      const [count] = await countQuery

      res.header('X-Total-Count', count["count"])


      const results = await query

      return res.json({images:results,pages:Math.ceil(count["count"]/total_per_page)})
    } catch(error) {
      next(error)
    }
  },
  async indexProcessed(req, res, next) {
    try {
      const total_per_page = 8

      const { area_id, page = 1 } = req.query
      const user_id = req.user.id
      
      const preQuery = await Area.query()
      .where({id: area_id})
      .limit(1)

      if(preQuery[0].user_id != user_id){
        return res.send({message:"Imagem inacessível"}) 
      }

      const query = knex('images_processed')
      .limit(total_per_page)
      .offset((page - 1) * total_per_page)

      const countQuery = knex('images_processed')
      .count()
      if(area_id){
        query
        .where({area_id})
        .join('areas', 'areas.id', '=', 'images_processed.area_id')
        .select('images_processed.*','areas.title')
        
        countQuery
        .where({area_id})
      }
      const [count] = await countQuery

      res.header('X-Total-Count', count["count"])
      
      
      const results = await query
      
      return res.json({images:results,pages:Math.ceil(count["count"]/total_per_page)})
    } catch(error) {
      next(error)
    }
  },
  async thumbnail(req, res, next) {
    try {

      const { area_id } = req.body
      const user_id = req.user.id
      
      const preQuery = await Area.query()
      .where({id: area_id})
      .limit(1)

      if(preQuery[0].user_id != user_id){
        return res.send({message:"Imagem inacessível"}) 
      }

      const query = Image.query().where({area_id:area_id})

      const results = await query

      return res.json(results)
    } catch(error) {
      next(error)
    }
  },
  async create(req, res, next) {
    try {
      const { link,meta_dados,area_id } = req.body

      const user_id = req.user.id
      const results = await Area.query()
      .where({id: area_id })
      .limit(1)

      if(results[0].user_id != user_id){
        return res.send({message:"Imagem não pôde ser criada"}) 
      }

      await Image.query().insert({
        link,
        meta_dados,
        area_id
      })
      return res.send({message:"Imagem criada com sucesso"})
    } catch(error) {
      next(error)
    }
  },
  async download(req, res, next) {
    try{
      const { id } = req.body
      const user_id = req.user.id
      const results = await Area.query()
      .where({ id })
      .limit(1)
      
      if(results.length === 0){
        return res.send({message:"Área não existe"})
      }
      
      if(results[0].user_id != user_id){
        return res.send({message:"Imagens não podem ser acessadas por você"})
      }      
      var arrayImgs = []
      await axios.post('http://127.0.0.1:5000/api/area/download', {geojson: results[0].geojson}).then(resposta => {
        console.log(resposta.data.imagens)

        for(const url of resposta.data.imagens){
          
          arrayImgs.push({
            link: url,
            meta_dados: JSON.stringify({'Nuvens':50}),
            area_id: id
          })

        }
        
      }).catch(error => {
        res.status(500).send({
          status : 0,
          erro : error,
          message : 'Erro ao efetuar Download da Imagem!'
        })
      })     
      
      knex.transaction(function(trx) {
      
        Image.query().insert(arrayImgs)
          .then(trx.commit)
          .catch(trx.rollback);
      })
      .then(function(inserts) {
        console.log(inserts.length + ' imagens foram inseridas no banco');
        return res.send({message:"Imagens foram baixadas"})
      })
      .catch(function(error) {
        // If we get here, that means that neither the 'Old Books' catalogues insert,
        // nor any of the books inserts will have taken place.
        console.error(error);
        return res.send({message:"Erro durante o download das imagens"})
      });


    } catch(error) {
      next(error)
    }
  },
  async process(req, res, next) {
    try{
      const { id, area_id } = req.body
      const user_id = req.user.id
      const results = await Area.query()
      .where({ id:area_id })
      .limit(1)
      
      if(results.length === 0){
        return res.send({message:"Área não existe"})
      }
      
      if(results[0].user_id != user_id){
        return res.send({message:"Imagens não podem ser acessadas por você"})
      }
      
      const url = await Image.query().where({id})

      console.log(url[0].link)

      var arrayImgs = []
      await axios.post('http://127.0.0.1:5000/api/area/processar', {url: url[0].link}).then(resposta =>{
        console.log(resposta.data.imagens)

        for(const url of resposta.data.imagens){
          
          arrayImgs.push({
            link: url,
            area_id: area_id
          })

        }
        
      }).catch(error => {
        console.log(error)
      })     
      
      await knex('images_processed').insert(arrayImgs)

      return res.send({message:"Imagens foram processadas"})

    } catch(error) {
      next(error)
    }
  },
}
