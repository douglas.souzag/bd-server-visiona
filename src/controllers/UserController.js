const knex = require('../database')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const User = require('.././models/UserModel.js');


module.exports = {
  async authenticate(req, res, next) {
    try {
      const {email, password} = req.body
      const results = await User.query().where({email}).limit(1)

      const admin = results[0].admin

      bcrypt.compare(password,results[0].password, (err, result)=>{
        if(err){
          return res.status(401).send({message:"Falha na autenticação"})
        }
        if(result){
          const token = jwt.sign({
            id: results[0].id,
            email: results[0].email,
            username: results[0].username,
            admin: admin
          },
          process.env.SECRET_KEY,
          {
            expiresIn: '24H'
          })
          
          return res.send({token:token,username:results[0].username,email:results[0].email})
        }
        return res.status(401).send({message:"Falha na autenticação"})
      })

    } catch(error) {
      next(error)
    }
  },
  // FUNCTION INDEX
  async index(req, res, next) {

    try {
      if(req.user.admin){
        const results = await User.query().select('id','username', 'email','admin', 'created_at','updated_at').from('users')
        return res.json(results)
      } else {
        return res.send({erro:'Você não tem permissão suficiente.'})
      }
    } catch(error) {
      next(error)
    }
  },
  // FUNCTION CREATE 
  async create(req, res, next) { 
    try {
      const { username,email,password } = req.body
      const hash = bcrypt.hashSync(password,10)
      
      await User.query().insert({
          username,
          email,
          password:hash
      })
      return res.send({message:"Usuário criado com sucesso"})
    } catch(error) {
      next(error)
    }
  },
  // FUNCTION UPDATE 
  async update(req, res, next){
    try {
      const { username,email } = req.body
      const id = req.user.id

      await User.query()
      .update({ username,email })
      .where({ id: id })

      return res.send({message:"Usuário atualizado"})
    } catch(error) {
      next(error)
    }
  },
  // FUNCTION DELETE 
  async delete(req, res, next){
    try{

      if(req.user.admin){

        const { id } = req.body
      
        await User.query()
        .where({ id })
        .del()
        
        return res.send({message:"Usuário excluido"})

      } else {
        const id = req.user.id
  
        await User.query()
        .where({ id })
        .del()
        
        return res.send({message:"Usuário excluido"})
      }

    } catch(error) {
      console.log(req.body)
      next(error)
    }
  }
}