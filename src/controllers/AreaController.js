const knex = require('../database')
const { json } = require('express')
const Area = require('.././models/AreaModel.js');

module.exports = {
  async index(req, res, next) {
    try {
      const total_per_page = 8

      const { page = 1 } = req.query
      const user_id = {user_id:req.user.id}
      const query = Area.query()
      .limit(total_per_page)
      .offset((page - 1) * total_per_page)

      const countQuery = Area.query()
      .count()

      if(user_id){
        query
        .where(user_id)
        .select('id','title','geojson','ha','cloudiness','period','satellite','thumbnail','created_at')
        
        countQuery
        .where(user_id)
      }
      const [count] = await countQuery

      res.header('X-Total-Count', count["count"])


      const results = await query
      return res.send({areas:results,pages:Math.ceil(count["count"]/total_per_page)})
    } catch(error) {
      next(error)
    }
  },
  async create(req, res, next) {     
    try {
      const { title,geojson,ha,period,cloudiness,satellite } = req.body
      await Area.query().insert({
        title,
        user_id: req.user.id,
        geojson: JSON.stringify(geojson),
        ha: parseFloat(ha),
        period: `${period}`,
        cloudiness: parseFloat(cloudiness),
        satellite,
      })
      return res.send({message:"Área criada com sucesso"})
    } catch(error) {
      console.log(error)
      next(error)
    }
  },
  async delete(req, res, next) {
    try{
      const { id } = req.params
      const user_id = req.user.id
      const results = await Area.query()
      .where({ id })
      .limit(1)
      
      
      if(results.length === 0){
        return res.send({message:"Área não existe"})
      }
      
      if(results[0].user_id != user_id){
        return res.send({message:"Área não pôde ser excluida"})
      }

      await Area.query()
      .where({id})
      .del()

      return res.send({message:"Área excluida"})

    } catch(error) {
      next(error)
    }
  },
  async getBigAreas(req, res, next) {
    try {
      const { sat, ha } = req.query
      const results = await Area.query()
      .first()
      .where({ satellite: sat })
      .andWhere( 'ha', '>', ha )

      let BigArea = results
      BigArea.created_at = `${BigArea.created_at}`

      const area_geojson = Area.fromJson(BigArea)

      const pojo = area_geojson.toJSON()
      
      if(results.length === 0){
        return res.send({
          status : 2,
          message : "Área não existe"
        })
      }

      return res.send({
        status : 1,
        message : 'Sucesso!',
        area : pojo.AreaGeoJSON
      })
    } catch (error) {
      res.status(500).send({
        status : 0,
        erro : `${error}`,
        message : 'Erro ao buscar por dados'
      })
    }
  }
}