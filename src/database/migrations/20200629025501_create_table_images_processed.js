exports.up = function(knex) {
    return knex.schema.createTable('images_processed', function(table){
      table.increments('id')
      table.text('link').notNullable()
      table.text('meta_dados').notNullable()
  
      table.integer('area_id')
          .references('areas.id')
          .notNullable()
          .onDelete('CASCADE')
  
      table.timestamp('created_at').defaultTo(knex.fn.now())
    })
  };
  exports.down = function(knex) {
    return knex.schema.dropTable('images_processed')
  };
  