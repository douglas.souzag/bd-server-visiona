exports.up = function(knex) {
  return knex.schema.createTable('areas', function(table){
    table.increments('id')
    table.text('title').notNullable()
    table.text('geojson').notNullable()
    table.text('period').notNullable()
    table.text('satellite').notNullable()
    table.text('thumbnail')
    table.float('cloudiness').notNullable()
    table.float('ha').notNullable()
    
    table.integer('user_id')
        .references('users.id')
        .notNullable()
        .onDelete('CASCADE')
    
    table.timestamp('created_at').defaultTo(knex.fn.now())
  })
};
exports.down = function(knex) {
  return knex.schema.dropTable('areas')
};
  