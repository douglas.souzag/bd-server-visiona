
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('areas').del().then(function () {
      // Inserts seed entries
    return knex('areas').insert([
      {
        user_id: 1,
        title: 'Area',
        geojson: [0,0,0,0]
      },
      {
        user_id: 2,
        title: 'Area',
        geojson: [0,0,0,0]
      },
      {
        user_id: 3,
        title: 'Area',
        geojson: [0,0,0,0]
      },
      {
        user_id: 4,
        title: 'Area',
        geojson: [0,0,0,0]
      },
      {
        user_id: 5,
        title: 'Area',
        geojson: [0,0,0,0]
      },
    ]);
  });
};
