
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('images').del().then(function () {
      // Inserts seed entries
    return knex('images').insert([
      {
        area_id: 1,
        link: 'teste.com.br',
        meta_dados: {'nuvens':50.0}
      },
    ]);
  });
};
