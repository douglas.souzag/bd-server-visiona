const bcrypt = require('bcrypt')

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del().then(function () {
    // Inserts seed entries
    return knex('users').insert([
      {
        username: 'Douglas',
        email: 'douglas.souzag@hotmail.com',
        password: bcrypt.hashSync('teste',10),
      },
      {
        username: 'Enzo',
        email: 'enzop.gerola@gmail.com',
        password: bcrypt.hashSync('teste',10),
      },
      {
        username: 'Gabriel',
        email: 'gsilva505.gs@gmail.com',
        password: bcrypt.hashSync('teste',10),
      },
      {
        username: 'Henrique',
        email: 'henrisalex@gmail.com',
        password: bcrypt.hashSync('teste',10),
      },
      {
        username: 'José',
        email: 'jose.elias.oliveira2017@gmail.com',
        password: bcrypt.hashSync('teste',10),
      }
    ]);
  });
};
