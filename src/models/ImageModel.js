const { Model } = require('objection');

const tableNames = require('.././constants/tableNames');
const schema = require('.././schemas/Image.json');

class ImageModel extends Model {
  static get tableName() {
    return tableNames.image;
  }

  static get jsonSchema() {
    return schema;
  }

  static get relationMappings() {
    const Area = require('./AreaModel')
    return {
      areas: {
        relation: Model.BelongsToOneRelation,
        modelClass: Area,

        join: {
          from: 'images.area_id',
          to: 'areas.id'
        }
      }
    }
  }
}

module.exports = ImageModel;