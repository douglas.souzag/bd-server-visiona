const { Model } = require('objection');

const tableNames = require('.././constants/tableNames');
const schema = require('.././schemas/Area.json');

class AreaModel extends Model {
  static get tableName() {
    return tableNames.area;
  }

  static get jsonSchema() {
    return schema;
  }

  static get virtualAttributes() {
    return ['AreaGeoJSON'];
  }

  AreaGeoJSON() {
    return {
      title : this.title,
      geojson : JSON.parse(this.geojson)
    };
  }

  static get relationMappings() {
    const User = require('./UserModel')
    const Image = require('./ImageModel')
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,

        join: {
          from: 'areas.user_id',
          to: 'users.id'
        }
      },
      images: {
        relation: Model.HasManyRelation,

        modelClass: Image,
        join: {
          from: 'areas.id',
          to: 'images.area_id'
        }
      },
    }
  }

}

module.exports = AreaModel;