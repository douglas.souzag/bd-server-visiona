const { Model } = require('objection');

const tableNames = require('.././constants/tableNames');
const schema = require('.././schemas/User.json');

class UserModel extends Model {
  static get tableName() {
    return tableNames.user;
  }

  static get jsonSchema() {
    return schema;
  }

  static get relationMappings() {
 
    const Area = require('./AreaModel')

    return {
      areas: {
        relation: Model.HasManyRelation,
        
        modelClass: Area,
        join: {
          from: 'users.id',
          to: 'areas.user_id'
        }
      },
    }
  }

}

module.exports = UserModel;