const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
    try{
        const decoded = jwt.verify(req.headers.x_access_token, process.env.SECRET_KEY)
        req.user = decoded
        next()
    } catch (error){
        return res.status(401).send({message:"Falha na autenticação"})
    }   
}