const swaggerDocument = require('./swagger.json');
const swaggerUi = require('swagger-ui-express')

const express = require('express')
const cors = require('cors');
const routes = require('./routes')

const app = express()
const port = 3333

app.use(express.json())
app.use(cors())
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api/v1', routes);

app.use((error,req,res,next) => {
    res.status(error.status || 500)
    res.json({ error: error.message })
})

app.listen(port, ()=> console.log('Server is running on port '+port))